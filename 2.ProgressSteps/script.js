const progress = document.getElementById("progress");
const prev = document.getElementById("prev");
const next = document.getElementById("next");
const circles = document.querySelectorAll(".circle");

let currentStep = 1;

next.addEventListener('click', () => {
    currentStep++;

    if(currentStep > circles.length) {
        currentStep = circles.length;
    }

    update();
})

prev.addEventListener('click', () => {
    currentStep--;

    if(currentStep < 1) {
        currentStep = 1;
    }

    update();
})

const update = () => {
    circles.forEach((circles, idx) => {
        if(idx < currentStep) {
            circles.classList.add('active');
            return;
        }
        else {
            circles.classList.remove('active');
            return;
        }
    })

    const actives = document.querySelectorAll('.active');
    console.log(actives.length, circles.length);

    progress.style.width = `${((actives.length - 1) / (circles.length - 1)) * 100}%`

    if(currentStep == 1) {
        prev.disabled = true;
    }
    else if(currentStep === circles.length) {
        next.disabled = true
    }
    else{
        prev.disabled = false;
        next.disabled = false;
    }
}